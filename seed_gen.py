import sys, time, re

termDict = {}
tagDict = {}
pB = re.compile('B-(\S+)')

def procSent(sent):
	lth = len(sent)
	#print >> sys.stderr, sent 
	bLst = []
	beg = 0
	lab = ''
	stat = 0
	for i, (word, tag) in enumerate(sent):
		m = pB.match(tag)
		if m:
			if stat == 1:
				bLst.append((beg, i, lab))
			beg = i
			lab = m.group(1)
			stat = 1
		if tag == 'O' and stat == 1:
			bLst.append((beg, i, lab))
			stat = 0
	if stat == 1:
		bLst.append((beg, len(sent), lab))
#	print >> sys.stderr, bLst
	for (beg, end, lab) in bLst:
		sta = 0
		if beg > 1: sta = beg - 2
		fin = lth 
		if end <= lth - 2: fin = end + 2
		for j in range(sta, fin):
		#	print >> sys.stderr, j
			idx = 0
			if j < beg:
				idx = j - beg
			if j >= end:
				idx = j - end + 1
			fea = sent[j][0] + '<'+str(idx)+'>'
			if fea in termDict:
				termDict[fea] += 1.0
			else:
				termDict[fea] = 1.0
			if (fea, lab) in tagDict:
				tagDict[fea, lab] += 1.0
			else:
				tagDict[fea, lab] = 1.0
def calc():
	slist = []
	for (fea, tag) in tagDict:
		if tag == 'O' or termDict[fea] < 100: continue
		slist.append((-tagDict[fea,tag]/termDict[fea], fea, tag, termDict[fea]))
	slist.sort()
	for (s, f, t, freq) in slist:
		print f + '\t' + t + '\t' + str(freq) + '\t' + str(-s)

if __name__ == "__main__":
	startT = time.clock()
	trainF = open(sys.argv[1], 'r')
	sentence = [] 
	for l in trainF:
		l = l.strip()
		if l == '':
			procSent(sentence)
			sentence = [] 
		else:
			sentence.append(l.split()) 
	calc()
	endT = time.clock()
