#! /usr/bin/python
import sys
import re
from type_checker import get_type
#arg1: the input file before replacement
#arg2: the output replaced file
#arg3: switch for the advanced replacement for the rare words 
if __name__ == "__main__":
	f_1 = open(sys.argv[1], 'r')
	f_2 = open(sys.argv[2], 'w')
	tbl1 = {'##O##':'O', '##B-problem##':'B-test', '##I-treatment##':'I-test', '##B-treatment##':'I-treatment', '##I-test##':'B-problem', '##B-test##':'B-treatment', '##I-problem##':'I-problem'}
	tbl2 = {'O':'##O##', 'B-problem':'##B-problem##', 'I-treatment':'##I-treatment##', 'B-treatment':'##B-treatment##', 'I-test':'##I-test##', 'B-test':'##B-test##', 'I-problem':'##I-problem##'}
	fs = f_1.read()
	f_1.close()
	for i in tbl2:
		fs = fs.replace(' ' + i + ' ', ' ' + tbl2[i] + ' ')
	for i in tbl1:
		fs = fs.replace(' ' + i + ' ', ' ' + tbl1[i] + ' ')
	f_2.write(fs)
	f_2.close()
