#! /usr/bin/python
import sys
import re
from type_checker import get_type
#arg1: the input file before replacement
#arg2: the output replaced file
#arg3: switch for the advanced replacement for the rare words 
if __name__ == "__main__":
	f = open(sys.argv[1], 'r')
	p = re.compile('\S+\s(\S+)\s(\S+)')
	tbl1 = {}
	tbl2 = {}
	c = 0
	for line in f:
		match = p.match(line)
		c = c + 1
		if match:
			t1 = match.group(1)
			if t1 in tbl1:
				tbl1[t1] = tbl1[t1] + 1
			else:
				tbl1[t1] = 1
			t2 = match.group(2)
			if t2 in tbl2:
				tbl2[t2] = tbl2[t2] + 1
			else:
				tbl2[t2] = 1
	l = []
	c = []
	for t in tbl1:
		l.append((tbl1[t], t))
	for t in tbl2:
		c.append((tbl2[t], t))
	l.sort()
	c.sort()
	print c
	print l
	f.close()
