#! /usr/bin/python
import sys
import re
#arg1: the input file before replacement
#arg2: the output replaced file
#arg3: switch for the advanced replacement for the rare words 
if __name__ == "__main__":
	f_in = open(sys.argv[1], 'r')
	f_out = open(sys.argv[2], 'w')
	tbl = {}
	for line in f_in:
		word = line.strip()
		if len(word) == 0: continue
		if word in tbl:
			tbl[word] = tbl[word] + 1 
		else:
			tbl[word] = 1
	f_in.seek(0, 0)
	for line in f_in:
		word = line.strip()
		if len(word) == 0:
			f_out.write('\n')
			continue
		#check the word frequency and process the replacement
		if tbl[word] < 5:
			f_out.write('_RARE_\n')
		else:
			f_out.write( word + '\n')
	f_in.close()
	f_out.close()
