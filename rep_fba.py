#! /usr/bin/python
import sys
import re
from type_checker import get_type
#arg1: the input file before replacement
#arg2: the output replaced file
#arg3: switch for the advanced replacement for the rare words 
#[(16193, '4'), (16564, '5'), (20807, '2'), (26375, '6'), (27841, '1'), (39580, '3'), (119947, '0')]
#[(7949, 'I-treatment'), (8012, 'I-test'), (9225, 'B-test'), (9343, 'B-treatment'), (12586, 'B-problem'), (17662, 'I-problem'), (202530, 'O')]
taglst = [] 
slst = ['0','1','2','3','5','6']
tset = set(['B-test','I-test','I-treatment','B-problem','B-treatment','I-problem'])
tb = {}
total = .0
maxi = .0
def search(tbl,i,tm):
	global maxi, tb
	if len(tm) == 0:
		print tbl
		corr = .0
		for (a,p) in taglst:
			if len(a) == len(tbl[p]):
				corr += 1
		if corr > maxi:
			maxi = corr
			tb = dict(tbl)
	else:
		s = slst[i]	
		for t in tm:
			tm1 = set(tm)
			tm1.remove(t)
			tbl[s] = t
			search(tbl,i+1,tm1)
		
if __name__ == "__main__":
	f_1 = open(sys.argv[1], 'r')
	f_2 = open(sys.argv[2], 'w')
	p = re.compile('\S+\s(\S+)\s(\S+)') 
	for l in f_1:
		m = p.match(l)
		if m:
			taglst.append((m.group(1),m.group(2)))
			total = total + 1
	search({'4':'O'}, 0, set(tset))
	p = re.compile('(\S+\s\S+\s)(\S+)')
	print tb
	print maxi/total
	f_1.seek(0,0)
	for l in f_1:
		m = p.match(l)
		if m:
			f_2.write(m.group(1) + tb[m.group(2)]+'\n')
	f_1.close()
	f_2.close()
