#! /usr/bin/python
import sys
import re
import random

if __name__ == "__main__":
	f_in = open(sys.argv[1], 'r')
	f_val = open(sys.argv[2], 'w')
	f_ran = open(sys.argv[3], 'w')
	f_sen = open(sys.argv[4], 'w')
	s = set()
	pattern = re.compile('(\S*)\s\S+\s(\S+)\s(\S+)')
	for line in f_in:
		match = pattern.match(line)
		if match:
			s.add(match.group(3))
			w = match.group(1)
			if w == '':
				w = '#no_words#'
			f_val.write(w + ' ' + match.group(3) + '\n')
			f_sen.write(w + '\n')
		else:
			f_val.write(line)
			f_sen.write(line)
	f_val.close()
	f_sen.close()
	f_in.seek(0,0)
	l = list(s)
	for line in f_in:
		match = pattern.match(line)
		if match:
			w = match.group(1)
			if w == '':
				w = '#no_words#'
			ran = random.randint(0,100)
			if ran < 62:
				f_ran.write(w + ' ' + 'O' + '\n')
			else:
				ranIdx = random.randint(0, len(s) - 1) 
				f_ran.write(w + ' ' + l[ranIdx] + '\n')
		else:
			f_ran.write(line)
	f_in.close()
	f_ran.close()
