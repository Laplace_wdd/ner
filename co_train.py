import sys, time, re

termDict = {}
tagDict = {}
rule = {}
doc = []
feaMap = {}
ruleSet = set()
bouMap = {} 
bouLst = []

def genBouLst():
	bLst = []
	beg = 0
	stat = 0
	for idx, tup in enumerate(doc):
		term = tup[0]
		tag = tup[1]
		pred = tup[2]
		if tag.startswith('B-'):
			if stat == 1:
				bouLst.append((beg, idx))
			beg = idx 
			stat = 1
		if (tag == 'O' or tag == '') and stat == 1:
			bouLst.append((beg, idx))
			stat = 0
	if stat == 1:
		bouLst.append((beg, sent[len(doc)-1][0]+1))

def genFea():
	for (beg, end) in bouLst:
		sta = beg
		while doc[sta][0] != '' and sta > beg - 2:
			sta -= 1
		fin = end 
		while doc[fin][0] != '' and fin < end + 2:
			fin += 1
		feaMap[beg,end] = set()
		for j in range(sta, fin):
			idx = 0
			if j < beg:
				idx = j - beg
			if j >= end:
				idx = j - end + 1
			fea = doc[j][0] + '<'+str(idx)+'>'
			feaMap[beg,end].add(fea)
			if fea in termDict:
				termDict[fea] += 1.0
			else:
				termDict[fea] = 1.0

def predict():
	for (beg, end) in bouLst:
		if (beg, end) in bouMap:continue
		feaSet = feaMap[beg, end]
		cand = 'O'
		maxi = 0
		for fea in feaSet:
			if fea in rule:
				(tmpl, score) = rule[fea]
				if score > maxi:
					maxi = score
					cand = tmpl
		if cand == 'O': continue
		doc[beg][2] = 'B-' + cand 
		for i in range(beg+1, end):
			doc[i][2] = 'I-' + cand 
		bouMap[beg, end] = cand

def update():
	tagDict = {}
	for (beg, end) in bouMap:
		p = bouMap[beg, end]
		s = feaMap[beg, end]
		for fea in s:
			if fea in ruleSet: continue
			if (fea, p) in tagDict:
				tagDict[fea, p] += 1.0
			else:
				tagDict[fea, p] = 1.0
	newRule = {}
	for (fea, tag) in tagDict:
		score = tagDict[fea,tag]/termDict[fea]
		if score > float(sys.argv[3]) and fea not in ruleSet:
			newRule[fea] = (tag, score)
			ruleSet.add(fea)
	rule = newRule

def loadDoc():
	trainF = open(sys.argv[1], 'r')
	sentence = [] 
	idx = 0
	for l in trainF:
		l = l.strip()
		if l == '':
			doc.append(['', '', ''])
		else:
			tup = l.split()
			term = tup[0]
			key = tup[1]
			pred = 'O' 
			doc.append([term, key, pred])
		idx += 1
	trainF.close()
	genBouLst()
	genFea()

def genRes():
	for tup in doc: 
		t = tup[0]
		k = tup[1]
		p = tup[2]
		if t == '' and k == '' and p == '':
			print
		else:
			print t + ' ' + k + ' ' + p
	sys.stdout.flush()

def loadSeed():
	seedF = open(sys.argv[2], 'r')
	for l in seedF:
		ent = l.split('\t')
		rule[ent[0]] = (ent[1], 1)
		ruleSet.add(ent[0])
	seedF.close()

if __name__ == "__main__":
	startT = time.clock()
	loadSeed()
	loadDoc()
	sentence = [] 
	for t in range(0, int(sys.argv[4])):
		predict()
		update()
	genRes()
	endT = time.clock()
