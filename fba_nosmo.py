#! /usr/bin/python
import sys, re, math, random, time
S = set() #the sate set
q = {} #transition prob
e = {} #emission prob
x = [] #input observation
alpha = {} #forward prob
beta = {} #backward prob
m = 0 #sentence length
e_n = {} #backup for e
q_n = {} #backup for q
likelihood = 0
#function for generating the tag set S and the emission
def pi(u,v,j):
	tss = random.random() 
	if (u,v) in q:
		tss = q[u,v]
	else:
		q[u,v] = tss
	emi = random.random() 
	if (v, x[j]) in e:
		emi = e[v,x[j]]
	else:
		e[v,x[j]] = emi
	return tss*emi 

def forward():
	global alpha
	alpha = {}
	for s in S:
		alpha[0,s] = pi(-1,s,0)
	for i in range(1, m):
		for v in S:
			base = 0
			for u in S:
				base = base + alpha[i-1,u]*pi(u,v,i)
			alpha[i,v] = base

def backward():
	global beta
	beta = {}
	for s in S:
		beta[m-1,s] = 1
	for j in range(1,m):
		i = m-1-j
		for v in S:
			base = 0
			for u in S:
				base = base + beta[i+1,u]*pi(v,u,i+1)
			beta[i,v] = base

#the fba algorithm
#x: the input sentence
#S: the tag set
#q: the bigram 
#w: the emission
#opt_type: type of replacement
def estep():
	global m
	global likelihood
	m = len(x)
	forward()
	backward()
	Z = 0
	mu = {}
	for s in S:
		Z = Z + alpha[m-1,s]
	likelihood = likelihood + math.log(Z)
	for j in range(0,m):
		for a in S:
			mu[j,a] = alpha[j,a]*beta[j,a]/Z
	for j in range(0,m-1):
		for a in S:
			for b in S:
				mu[j,a,b] = alpha[j,a]*pi(a,b,j+1)*beta[j+1,b]/Z
	for a in S:
		if -1 in q_n:
			if a in q_n:
				q_n[-1][a] = q_n[-1][a] + mu[0,a]
			else:
				q_n[-1][a] = mu[0,a]
		else:
			q_n[-1] = {a:mu[0,a]}
	for a in S:
		for b in S:
			nume = 0
			for i in range(0, m-1):
				nume = nume + mu[i,a,b]
			if a in q_n:
				q_n[a][b] = nume
			else:
				q_n[a] = {b:nume}
	for a in S:
		for i in range(0,m):
			if a in e_n:
				if x[i] in e_n[a]:
					e_n[a][x[i]] = e_n[a][x[i]] + mu[i,a] 
				else:
					e_n[a][x[i]] = mu[i,a]
			else:
				e_n[a] = {x[i]:mu[i,a]}

smooth = 0.01 
	
def	mstep():
	global e
	global q
	global e_n
	global q_n
	e = {} 
	q = {} 
	for a in q_n:
		base = 0
		for b in q_n[a]:
			base = base + q_n[a][b] + smooth 
		for b in q_n[a]:
			if base == 0:
				base = 1
			q[a,b] = (q_n[a][b] + smooth)/base
	for a in e_n:
		base = 0
		for w in e_n[a]:
			base = base + e_n[a][w] +smooth 
		for w in e_n[a]:
			if base == 0:
				base = 1
			e[a,w] = (e_n[a][w] + smooth)/base
	q_n = {}
	e_n = {}

def genPara(trans, emission):
	transF = open(trans, 'w')
	for (u,v) in q:
		transF.write(str(u) + ' ' + str(v) + ' ' + str(math.log(q[u,v])) + '\n')
	transF.close()
	emiF = open(emission, 'w')
	for (v,w) in e:
		emiF.write(str(math.log(e[v,w])) + ' ' + str(v) + ' ' + str(w) + '\n')
	emiF.close()

#arg1: log emission probability of words and tags	
#arg2: log trigram probability 
#arg3: the input set of sentences
#arg4: the result words with tages
#arg5: the type of replacement
if __name__ == "__main__":
	startT = time.clock()
	passage = open(sys.argv[1], 'r')
	stats = open(sys.argv[4], 'w')
	count_thr = int(sys.argv[5])
	cnt = 0
	x = ['##START##']
	S = set(range(0,7))
	old_likelihood = 1
	itr = 0
	thr = 0.0001
	sl = 15
	nor = 40 
	while(itr < nor):
		old_likelihood = likelihood
		likelihood = 0
		while(1):
			line = passage.readline()
			if not line:
				break
			line=line.strip('\n')
			if(len(line) == 0):
				x.append('##STOP##')
				#go E-step
				if len(x) < sl:
					estep()
				cnt = cnt + 1
				x = ['##START##']
				if count_thr != -1 and cnt >= count_thr:
					cnt = 0
					break
			else:
				x.append(line)
		passage.seek(0,0)
		#go M-step
		mstep()
		itr = itr + 1
		stats.write(str(itr) + ' ' + str(likelihood) + '\n')
		print str(itr) + ' ' + str(likelihood)
	endT = time.clock()
	genPara(sys.argv[2], sys.argv[3])
	passage.close()
	stats.close()
	print 'Processing Time:' + str((endT - startT)) + 'sec'
