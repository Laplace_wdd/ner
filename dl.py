#! /usr/bin/python
from itertools import *
import sys, os, re, time
signature = {'I-test':{},'B-test':{},'I-treatment':{},'B-treatment':{},'I-problem':{}, 'B-problem':{}}
spell = {'I-test':{},'B-test':{},'I-treatment':{},'B-treatment':{},'I-problem':{}, 'B-problem':{}}
wrdCnt = {}
def update(sent):
	for i in range(0, len(sent)):
		x = sent[i][0]
		if not tag.startwith('N'):continue
		if x in spell[tag]:
			spell[tag][x] += 1
		else:
			spell[tag] = {x:1.0}
		if x in wrdCnt:
			wrdCnt[x] += 1
		else:
			wrdCnt[x] = 1.0
		pos = sent[i][1]
		tag = sent[i][2]
		if i > 0:
			xn = sent[i-1][0]
			if xn in signature[tag]:
				signature[tag][xn] += 1
			else:
				signature[tag] = {xn:1.0}
			if i > 1:
			xn = sent[i-2][0]
				if xn in signature[tag]:
					signature[tag][xn] += 1
				else:
					signature[tag] = {xn:1.0}
		if i+1 < len(sent):
			xn = sent[i+1][0]
			if xn in signature[tag]:
				signature[tag][xn1] += 1
			else:
				signature[tag] = {xn1:1.0}
			if i+2 < len(sent):
			xn = sent[i+2][0]
				if xn in signature[tag]:
					signature[tag][xn] += 1
				else:
					signature[tag] = {xn:1.0}
		
if __name__ == "__main__":
	start = time.clock()
	pn = re.compile('(.*)\t.*\t(.*)\t(.*)')
	testF = open(sys.argv[1], 'r')
	predF = open(sys.argv[2], 'w')
	sentence = []
	for l in testF:
		line = line.strip('\n')
		m = pn.match(l)	
		if m:
			sentence.append((m.group(1),m.group(2),m.group(3)))
		else:
			f_out.write(viterbi(sentence, val_tags, tag_set, bi_tbl, prior_tbl) + '\n')
			sentence = []

	testF.close()
	predF.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
