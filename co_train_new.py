import sys, time, re

termDict = {}
tagDict = {}
rule = {}
doc = []
spFea = {}
ctFea = {}
ruleSet = set()
bouMap = {} 
bouLst = []
allcap = re.compile('[A-Z]+')
allper = re.compile('.\..')
pblmDict = set()
pblmTDict = set()
tstDict = set()
tstTDict = set()
trtmntDict = set()
trtmntTDict = set()

def genBouLst():
	bLst = []
	beg = 0
	stat = 0
	for idx, tup in enumerate(doc):
		term = tup[0]
		tag = tup[1]
		pred = tup[2]
		if tag.startswith('B-'):
			if stat == 1:
				bouLst.append((beg, idx))
			beg = idx 
			stat = 1
		if (tag == 'O' or tag == '') and stat == 1:
			bouLst.append((beg, idx))
			stat = 0
	if stat == 1:
		bouLst.append((beg, sent[len(doc)-1][0]+1))

def genFea():
	for (beg, end) in bouLst:
		sta = beg
		while doc[sta][0] != '' and sta > beg - 2:
			sta -= 1
		fin = end 
		while doc[fin][0] != '' and fin < end + 2:
			fin += 1
		spFea[beg,end] = set()
		ctFea[beg,end] = set()
		fullString = ''
		for j in range(beg, end):
			fullString += doc[j][0] + ' '
		if fullString in pblmDict:
			addFea(fullString+'<pblm>', termDict)
			spFea[beg,end].add(fullString+'<pblm>')
		if fullString in tstDict:
			addFea(fullString+'<tst>', termDict)
			spFea[beg,end].add(fullString+'<tst>')
		if fullString in trtmntDict:
			addFea(fullString+'<trtmnt>', termDict)
			spFea[beg,end].add(fullString+'<trtmnt>')
		fullString += '<fullString>'
		addFea(fullString, termDict)
		spFea[beg,end].add(fullString)
		for j in range(sta, fin):
			idx = 0
			if j < beg:
				idx = j - beg
			if j >= end:
				idx = j - end + 1
			fea = doc[j][0] + '<'+str(idx)+'>'
			feaType = doc[j][2] + '<#FT#'+str(idx)+'>'
			nonalpha = re.subn('[A-Za-z]+', '', doc[j][0])[0] + '<nonalpha>'
			if nonalpha != '':
				spFea[beg,end].add(nonalpha)
			addFea(fea, termDict)
			addFea(feaType, termDict)
			addFea(nonalpha, termDict)
			if idx == 0:
				spFea[beg,end].add(fea)
				if allcap.match(doc[j][0]):
					spFea[beg,end].add('<allcap>')
					addFea('<allcap>', termDict)
				if allper.match(doc[j][0]):
					spFea[beg,end].add('<allper>')
					addFea('<allper>', termDict)
				if doc[j][0] in pblmDict:
					addFea(fea+'<pblm>', termDict)
					spFea[beg,end].add(fea+'<pblm>')
				if doc[j][0] in tstDict:
					addFea(fea+'<tst>', termDict)
					spFea[beg,end].add(fea+'<tst>')
				if doc[j][0] in trtmntDict:
					addFea(fea+'<trtmnt>', termDict)
					spFea[beg,end].add(fea+'<trtmnt>')
			else:
				ctFea[beg,end].add(fea)
				ctFea[beg,end].add(feaType)
				if doc[j][0] in pblmDict:
					addFea(fea+'<pblm>', termDict)
					ctFea[beg,end].add(fea+'<pblm>')
				if doc[j][0] in tstDict:
					addFea(fea+'<tst>', termDict)
					ctFea[beg,end].add(fea+'<tst>')
				if doc[j][0] in trtmntDict:
					addFea(fea+'<trtmnt>', termDict)
					ctFea[beg,end].add(fea+'<trtmnt>')

def addFea(fea, tdict):
	if fea in tdict:
		tdict[fea] += 1.0
	else:
		tdict[fea] = 1.0

def predict(feaDict):
	for (beg, end) in bouLst:
		if (beg, end) in bouMap:continue
		feaSet = feaDict[beg, end]
		cand = 'O'
		maxi = 0
		for fea in feaSet:
			if fea in rule:
				(tmpl, score) = rule[fea]
				if score > maxi:
					maxi = score
					cand = tmpl
		if cand == 'O': continue
		doc[beg][2] = 'B-' + cand 
		for i in range(beg+1, end):
			doc[i][2] = 'I-' + cand 
		bouMap[beg, end] = cand

def update(feaDict):
	tagDict = {}
	for (beg, end) in bouMap:
		p = bouMap[beg, end]
		s = feaDict[beg, end]
		for fea in s:
			if fea in ruleSet: continue
			if (fea, p) in tagDict:
				tagDict[fea, p] += 1.0
			else:
				tagDict[fea, p] = 1.0
	newRule = {}
	for (fea, tag) in tagDict:
		score = tagDict[fea,tag]/termDict[fea]
		if score > float(sys.argv[3]) and fea not in ruleSet:
			newRule[fea] = (tag, score)
			ruleSet.add(fea)
	rule = newRule

def loadDoc():
	trainF = open(sys.argv[1], 'r')
	sentence = [] 
	idx = 0
	for l in trainF:
		l = l.strip()
		if l == '':
			doc.append(['', '', '', ''])
		else:
#			print >> sys.stderr, l
			tup = l.rsplit('\t')
			lentup = len(tup)
			term = ''
			if lentup > 3:
				term = tup[lentup-4]
			key = tup[lentup-1]
			attr = tup[lentup-2]
			pred = 'O' 
			doc.append([term, key, pred, attr])
		idx += 1
	trainF.close()
	genBouLst()
	genFea()

def genRes():
	resF = open(sys.argv[8], 'r')
	for tup in doc: 
		t = tup[0]
		k = tup[1]
		p = tup[2]
		if t == '' and k == '' and p == '':
			print >> resF, 
		else:
			print >> resF, t + ' ' + k + ' ' + p
	resF.close()

def loadSeed():
	seedF = open(sys.argv[2], 'r')
	for l in seedF:
		ent = l.split('\t')
		rule[ent[0]] = (ent[1], 1)
		ruleSet.add(ent[0])
	seedF.close()

def loadDict():
	pblmF = open(sys.argv[5], 'r')
	tstF = open(sys.argv[6], 'r')
	trtmntF = open(sys.argv[7], 'r')
	lp = re.compile(r'(.*)\(.*\)')
	tmpPblmDict, tmpPblmTDict, tmpTstDict, tmpTstTDict, tmpTrtmntDict, tmpTrtmntTDict = ({},{},{},{},{},{})
	for l in pblmF:
		target = l.strip()
		if lp.match(target):
			target = lp.match(target).group(1)
		tmpPblmDict.add(target)
		tmpPblmTDict.update(target.split())
	for l in tstF:
		target = l.strip()
		if lp.match(target):
			target = lp.match(target).group(1)
		tmpTstDict.add(target)
		tmpTstTDict.update(target.split())
	for l in trtmntF:
		target = l.strip()
		if lp.match(target):
			target = lp.match(target).group(1)
		tmpTrtmntDict.add(target)
		tmpTrtmntTDict.update(target.split())
	pblmDict = tmpPblmDict-tmpTstDict-tmpTrtmntDict
	tstDict = tmpTstDict-tmpPblmDict-tmpTrtmntDict
	trtmntDict = tmpTrtmntDict-tmpPblmDict-tmpTstDict

	pblmTDict = tmpPblmTDict-tmpTstTDict-tmpTrtmntTDict
	tstTDict = tmpTstTDict-tmpPblmTDict-tmpTrtmntTDict
	trtmntTDict = tmpTrtmntTDict-tmpPblmTDict-tmpTstTDict

	pblmF.close()
	tstF.close()
	trtmntF.close()

if __name__ == "__main__":
	startT = time.clock()
	loadDict()
	loadSeed()
	loadDoc()
	sentence = [] 
	for t in range(0, int(sys.argv[4])):
		predict(spFea)
		update(ctFea)
		predict(ctFea)
		update(spFea)
	genRes()
	endT = time.clock()
