#! /usr/bin/python
import sys
import re
from type_checker import get_type
#arg1: the input file before replacement
#arg2: the output replaced file
#arg3: switch for the advanced replacement for the rare words 
if __name__ == "__main__":
	f_1 = open(sys.argv[1], 'r')
	f_2 = open(sys.argv[2], 'r')
	f_3 = open(sys.argv[3], 'w')
	p = re.compile('(\S+)\s(\S+)')
	for l1 in f_1:
		l2 = f_2.readline();
		m1 = p.match(l1)
		m2 = p.match(l2)
		if m1:
			f_3.write(m1.group(1) + '\t' + m1.group(2) + '\t' + m2.group(2) + '\n')
		else:
			f_3.write(l1)
	f_1.close()
	f_2.close()
	f_3.close()
