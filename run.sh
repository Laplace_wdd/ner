#!/bin/bash  
i=0
while [ $i -lt $1 ]
do  
	python replace.py data/input.dat data/train.dat no_opt
	python count_freqs.py data/train.dat > data/train.counts
	python gen_prior.py data/train.counts data/emi.dat
	python gen_tri.py data/train.counts data/tri.dat
	python viterbi_tagger.py data/emi.dat data/tri.dat data/sen.dat data/input.dat no_opt
	let i++  
	echo $i
done 
