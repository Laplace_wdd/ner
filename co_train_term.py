import sys, time, re

termDict = {}
tagDict = {}
rule = {}
doc = []
feaMap = {}
ruleSet = set()

def genFea(sent):
	lth = len(sent)
	#print >> sys.stderr, sent 
	bLst = []
	beg = 0
	lab = ''
	stat = 0
	for (idx, tag, word) in sent:
		if tag.startswith('B-'):
			if stat == 1:
				bLst.append((beg, idx))
			beg = idx 
			stat = 1
		if tag == 'O' and stat == 1:
			bLst.append((beg, idx))
			stat = 0
	if stat == 1:
		bLst.append((beg, sent[len(sent)-1][0]+1))
#	print >> sys.stderr, bLst
	for (beg, end, lab) in bLst:
		sta = 0
		if beg > 1: sta = beg - 2
		fin = lth 
		if end <= lth - 2: fin = end + 2
		feaMap[beg,end] = set()
		for j in range(sta, fin):
		#	print >> sys.stderr, j
			idx = 0
			if j < beg:
				idx = j - beg
			if j >= end:
				idx = j - end + 1
			fea = sent[j][0] + '<'+str(idx)+'>'
			feaMap[beg,end].add(fea)
			if fea in termDict:
				termDict[fea] += 1.0
			else:
				termDict[fea] = 1.0

#def genFea(sent):
#	lth = len(sent)
#	for i, (idx, word) in enumerate(sent):
#		sta = 0
#		if i > 1: sta = i - 2
#		fin = lth - 1 
#		if i < lth - 2: fin = i + 2
#		feaMap[idx] = set()
#		for j in range(sta, fin + 1):
#			fea = sent[j][1] + '<'+str(i-j)+'>'
#			feaMap[idx].add(fea)
#			if fea in termDict:
#				termDict[fea] += 1
#			else:
#				termDict[fea] = 1

def predict(sent):
	bLst = []
	beg = 0
	lab = ''
	stat = 0
	for (idx, tag, pred) in sent:
		if tag.startswith('B-'):
			if stat == 1:
				bLst.append((beg, idx))
			beg = idx 
			stat = 1
		if tag == 'O' and stat == 1:
			bLst.append((beg, idx))
			stat = 0
	if stat == 1:
		bLst.append((beg, sent[len(sent)-1][0]+1))
	lth = len(sent)
	for i, (idx, tag) in enumerate(sent):
		if tag != 'O':continue
		feaSet = feaMap[idx]
		cand = 'O'
		maxi = 0
		for fea in feaSet:
			if fea in rule:
				(tmpl, score) = rule[fea]
				if score > maxi:
					maxi = score 
					cand = tmpl
		doc[idx][2] = cand 

def update():
	for i, tup in enumerate(doc):
		t = tup[0]
		k = tup[1]
		p = tup[2]
		if p == 'O' or (t == '' and k == '' and p == ''): continue
		s = feaMap[i]
		for fea in s:
			if fea in ruleSet: continue
			if (fea, p) in tagDict:
				tagDict[fea, p] += 1.0
			else:
				tagDict[fea, p] = 1.0
	newRule = {}
	for (fea, tag) in tagDict:
		score = tagDict[fea,tag]/termDict[fea]
		if score > float(sys.argv[3]) and fea not in ruleSet:
			newRule[fea] = (tag, score)
			ruleSet.add(fea)
	rule = newRule
			
def loadDoc():
	trainF = open(sys.argv[1], 'r')
	sentence = [] 
	idx = 0
	for l in trainF:
		l = l.strip()
		if l == '':
			doc.append(['', '', ''])
			genFea(sentence)
			sentence = [] 
		else:
			tup = l.split()
			term = tup[0]
			key = tup[1]
			pred = 'O' 
			doc.append([term, key, pred])
			sentence.append((idx, key, term))
		idx += 1
	trainF.close()

def genRes():
	for tup in doc: 
		t = tup[0]
		k = tup[1]
		p = tup[2]
		if t == '' and k == '' and p == '':
			print
		else:
			print t + ' ' + k + ' ' + p
	sys.stdout.flush()

def loadSeed():
	seedF = open(sys.argv[2], 'r')
	for l in seedF:
		ent = l.split('\t')
		rule[ent[0]] = (ent[1], 1)
		ruleSet.add(ent[0])
	seedF.close()

if __name__ == "__main__":
	startT = time.clock()
	loadSeed()
	loadDoc()
	sentence = [] 
	for t in range(0, 5):
		for i, tup in enumerate(doc):
			term = tup[0]
			key = tup[1]
			pred = tup[2]
			if term == '' and key == '' and pred == '':
				predict(sentence)
				sentence = [] 
			else:
				sentence.append((i, key, pred)) 
		update()
	genRes()
	endT = time.clock()
