#! /usr/bin/python
import sys
import re
import math
from type_checker import get_type
#function for generating the tag set S and the emission
def gen_prior(inputf):
	f_in = open(inputf, 'r')
	tbl_prior = {}	
	taggs = set(['-1'])
	pattern = re.compile('(\S+)\s(\S+)\s(\S+)')
	for line in f_in:
		match = pattern.match(line)
		if match:
			word = match.group(3)
			tag = match.group(2)
			taggs.add(tag)
			val = float(match.group(1))
			if word in tbl_prior:
				tbl_prior[word][tag] = val
			else:
				tbl_prior[word] = {tag:val}
	f_in.close()
	return (tbl_prior, taggs)

#function for generating the trigrams
def gen_bi(inputf):
	f_in = open(inputf, 'r')
	tbl = {}
	pattern = re.compile('(\S+)\s(\S+)\s(\S+)')
	for line in f_in:
		match = pattern.match(line)
		if match:
			tbl[match.group(1),match.group(2)] = float(match.group(3))
	f_in.close()
	return tbl

#the viterbi algorithm
#x: the input sentence
#S: the tag set
#q: the trigram
#w: the emission
#opt_type: type of replacement
def viterbi(x, t, S, q, e):
	n = len(x)
	if n == 0:
		return ''
	#initialization for the DP
	pi = {(-1,'-1'):0}
	trace = {(-1,'-1'):[]}
	for v in S:
		if v == '-1':
			continue
		pi[-1,v] = float('-inf') 
		trace[-1,v] = []
	for k in range(0, n): 
		x_k = x[k]
		if x_k not in e:
			x_k = '_RARE_'
		for v in S:
			if v == '-1':continue
			max_prob = float('-inf');
			max_w = ''
			e_xv = float('-inf')
			if x_k in e:
				if v in e[x_k]:
					e_xv = e[x_k][v]
			for w in S:
				q_wv = float('-inf')
				if (w,v) in q:
					q_wv = q[w,v]
				pi_kw = float('-inf')
				if (k-1,w) in pi:
					pi_kw = pi[k-1,w]
#				print 'k:' + str(k) + ' w:' + w + ' v:' + v + ' x_k:' + x[k] + ' q_wv:' + str(q_wv) + ' pi_kw:' + str(pi_kw) +' e_xv:' + str(e_xv) 
				#update the new probability
				tmp_prob = pi_kw + q_wv + e_xv
				#find argmax_w
				if tmp_prob > max_prob:
					max_prob = tmp_prob
					max_w = w
			tl = list(trace[k-1,max_w])
			tl.append((max_w,max_prob))
			trace[k,v] = tl 
			pi[k,v] = max_prob
	cand_v = ''
	max_prob = float('-inf')
	for v in S:
		if (n-1,v) in pi:
			tmp_prob = pi[n-1,v]
			if tmp_prob > max_prob:
				max_prob = tmp_prob
				cand_v = v
	taglist = list(trace[n-1,cand_v])
	if cand_v == '':
		cand_v = '.'
	taglist.append((cand_v, 'misc'))
	#find the trace of the tags
	out = ''
	for k in range(0, n - 1):
		out = out + x[k] + ' ' + t[k] + ' '+ str(taglist[k+1][0]) + ' ' + str(taglist[k][1]) + '\n'
	out = out + x[n - 1] + ' ' + t[n - 1] + ' ' + str(cand_v) + ' ' + str(max_prob) + '\n'
	return out
#arg1: log emission probability of words and tags	
#arg2: log trigram probability 
#arg3: the input set of sentences
#arg4: the result words with tages
#arg5: the type of replacement
if __name__ == "__main__":
	(prior_tbl, tag_set) = gen_prior(sys.argv[1])
	bi_tbl = gen_bi(sys.argv[2]) 
	passage = open(sys.argv[3], 'r')
	f_out = open(sys.argv[4], 'w')
	sentence = []
	val_tags = []
	for line in passage:
		line=line.strip('\n')
		if(len(line) == 0):
			#run the viterbi algorithm
			f_out.write(viterbi(sentence, val_tags, tag_set, bi_tbl, prior_tbl) + '\n')
			sentence = []
			val_tags = []
		else:
			pair = line.split(' ')
			sentence.append(pair[0])
			val_tags.append(pair[1])
	passage.close()
	f_out.close()
