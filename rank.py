#! /usr/bin/python
import sys
import re
from type_checker import get_type
#arg1: the input file before replacement
#arg2: the output replaced file
#arg3: switch for the advanced replacement for the rare words 
if __name__ == "__main__":
	f_1 = open(sys.argv[1], 'r')
	f_2 = open(sys.argv[2], 'r')
	p = re.compile('\S+\s(\S+)')
	tbl1 = {}
	c = 0
	for line in f_1:
		match = p.match(line)
		c = c + 1
		if match:
			t = match.group(1)
			if t in tbl1:
				tbl1[t] = tbl1[t] + 1
			else:
				tbl1[t] = 1
	l = []
	for t in tbl1:
		l.append((tbl1[t], t))
	l.sort()
	print c
	print l
	c = 0
	tbl2 = {}
	for line in f_2:
		c = c + 1
		match = p.match(line)
		if match:
			t = match.group(1)
			if t in tbl2:
				tbl2[t] = tbl2[t] + 1
			else:
				tbl2[t] = 1
	l = []
	for t in tbl2:
		l.append((tbl2[t], t))
	l.sort()
	print c
	print l
	f_1.close()
	f_2.close()
